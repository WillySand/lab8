from django.test import TestCase,Client
from django.urls import resolve
from .views import home
from django.apps import apps
from .apps import Story8Config
   

class Lab8UnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(Story8Config.name, 'Story8')
        self.assertEqual(apps.get_app_config('Story8').name, 'Story8')
    def test_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)#test existing url
        response = Client().get('/admin')
        self.assertNotEqual(response.status_code, 200)#test url that doesn't exist
        response = Client().get('/home')
        self.assertNotEqual(response.status_code, 200)
    def test_function(self):
        foundFunc = resolve('/')
        self.assertEqual(foundFunc.func, home)
